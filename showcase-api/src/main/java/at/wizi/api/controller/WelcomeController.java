package at.wizi.api.controller;

import at.wizi.api.model.Welcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping(WelcomeController.MAPPING_WELCOME)
public class WelcomeController {

	public static final String MAPPING_WELCOME = "/welcome";

	private static final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public HttpEntity<Welcome> welcome(@RequestParam(defaultValue = "wizi") String name) {
		Welcome welcome = new Welcome("welcome ", name, LocalDateTime.now());

		logger.debug("{}", welcome);

		return new HttpEntity<>(welcome);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public HttpEntity<Welcome> welcome(@RequestBody @Valid Welcome welcome) {
		logger.debug("{}", welcome);
		return new HttpEntity<>(welcome);
	}
}
