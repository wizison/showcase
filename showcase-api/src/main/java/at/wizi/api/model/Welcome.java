package at.wizi.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"message", "name", "date"})
public class Welcome {
	@NotEmpty
	private String message;
	@NotEmpty
	private String name;
	@NotNull
	private LocalDateTime date;
}
