package at.wizi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ObjectMappingTest {

	private static final Logger logger = LoggerFactory.getLogger(ObjectMappingTest.class);

	@Getter
	@Setter
	@AllArgsConstructor
	class InnerData implements Serializable {
		private String name;
		private Long amount;
	}

	@Getter
	@Setter
	class Data implements Serializable {
		private String name;
		private List<String> stringValues;
		private Map<Long, InnerData> keyValueStore;
	}

	@Getter
	@Setter
	class DataDto {
		private String name;
		private List<String> stringValues;
		private Map<Long, InnerData> keyValueStore;
	}

	private Data buildDataObject() {
		Data data = new Data();
		data.setName("the name");
		data.setStringValues(Arrays.asList("one", "two", "three"));
		data.setKeyValueStore(new HashMap<>());
		data.getKeyValueStore().put(17L, new InnerData("innername", 1337L));

		return data;
	}

	@Test
	public void mapAnObjectTest() {
		Data data = buildDataObject();
		DataDto dataDto = new DataDto();

		BeanUtils.copyProperties(data, dataDto);

		assertThat(dataDto).isNotNull();
		InnerData innerData = dataDto.getKeyValueStore().get(17L);
		innerData.setName("new innername");

		logger.debug("fine");
	}
}
