package at.wizi.api.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collections;

public abstract class AbstractControllerTest {
	protected <T> HttpEntity<T> buildHttpEntity(T entityObject) {

		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);

		return new HttpEntity<>(entityObject, httpHeaders);
	}
}
