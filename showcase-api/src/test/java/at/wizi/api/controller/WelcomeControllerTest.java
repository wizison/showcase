package at.wizi.api.controller;

import at.wizi.api.ApiApplication;
import at.wizi.api.model.Welcome;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WelcomeControllerTest extends AbstractControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(WelcomeControllerTest.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void welcomeGetTest() {
		ResponseEntity<Welcome> responseEntity = restTemplate.exchange(WelcomeController.MAPPING_WELCOME, HttpMethod.GET, buildHttpEntity(null), Welcome.class);
		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
		assertThat(responseEntity.getBody().getName()).isNotEmpty();

		logger.debug("{}", responseEntity.getBody());
	}

	@Test
	public void welcomePostTest() {
		Welcome welcome = new Welcome("hello", "awesome dude", LocalDateTime.now());
		ResponseEntity<Welcome> responseEntity = restTemplate.exchange(WelcomeController.MAPPING_WELCOME, HttpMethod.POST, buildHttpEntity(welcome), Welcome.class);
		assertThat(responseEntity).isNotNull();
		assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
		assertThat(responseEntity.getBody()).isEqualToComparingFieldByField(welcome);

		logger.debug("{}", responseEntity.getBody());
	}
}
