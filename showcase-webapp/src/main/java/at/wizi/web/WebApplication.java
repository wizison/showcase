package at.wizi.web;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WebApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext context =
				new SpringApplicationBuilder(WebApplication.class)
						.web(true)
						.run(args);
	}
}
