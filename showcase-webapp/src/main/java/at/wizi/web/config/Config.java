package at.wizi.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;

@Configuration
@ComponentScan(basePackages = {"at.wizi.web"})
public class Config {
	private static final int SEED_BYTE_SIZE = 32;

	@Bean
	public PasswordEncoder passwordEncoder() {
		SecureRandom random = new SecureRandom(new byte[SEED_BYTE_SIZE]);
		return new BCryptPasswordEncoder(10, random);
	}
}
